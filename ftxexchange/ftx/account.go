package ftx

import (
	"ftxbot/ftxexchange/ftx/structs"
	"log"
)

func (client *FtxClient) GetPositions() (structs.Positions, error) {
	var positions structs.Positions
	resp, err := client._get("positions?showAvgPrice=true", []byte(""))
	if err != nil {
		log.Printf("Error GetPositions", err)
		return positions, err
	}
	err = _processResponse(resp, &positions)
	return positions, err
}

func (client *FtxClient) GetAccount() (structs.Account, error) {
	var account structs.Account
	resp, err := client._get("account", []byte(""))
	if err != nil {
		log.Printf("Error GetAccount", err)
		return account, err
	}
	err = _processResponse(resp, &account)
	return account, err
}
