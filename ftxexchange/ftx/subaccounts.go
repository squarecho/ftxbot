package ftx

import (
	"encoding/json"
	"ftxbot/ftxexchange/ftx/structs"
	"log"
)

func (client *FtxClient) GetSubaccounts() (structs.SubaccountsList, error) {
	var subaccounts structs.SubaccountsList
	resp, err := client._get("subaccounts", []byte(""))
	if err != nil {
		log.Printf("Error GetSubaccounts", err)
		return subaccounts, err
	}
	err = _processResponse(resp, &subaccounts)
	return subaccounts, err
}

func (client *FtxClient) CreateSubaccount(nickname string) (structs.Subaccount, error) {
	var subaccount structs.Subaccount
	requestBody, err := json.Marshal(map[string]string{"nickname": nickname})
	if err != nil {
		log.Printf("Error CreateSubaccount", err)
		return subaccount, err
	}
	resp, err := client._post("subaccounts", requestBody)
	if err != nil {
		log.Printf("Error CreateSubaccount", err)
		return subaccount, err
	}
	err = _processResponse(resp, &subaccount)
	return subaccount, err
}

func (client *FtxClient) ChangeSubaccountName(nickname string, newNickname string) (structs.Response, error) {
	var changeSubaccount structs.Response
	requestBody, err := json.Marshal(map[string]string{"nickname": nickname, "newNickname": newNickname})
	if err != nil {
		log.Printf("Error ChangeSubaccountName", err)
		return changeSubaccount, err
	}
	resp, err := client._post("subaccounts/update_name", requestBody)
	if err != nil {
		log.Printf("Error ChangeSubaccountName", err)
		return changeSubaccount, err
	}
	err = _processResponse(resp, &changeSubaccount)
	return changeSubaccount, err
}

func (client *FtxClient) DeleteSubaccount(nickname string) (structs.Response, error) {
	var deleteSubaccount structs.Response
	requestBody, err := json.Marshal(map[string]string{"nickname": nickname})
	if err != nil {
		log.Printf("Error DeleteSubaccount", err)
		return deleteSubaccount, err
	}
	resp, err := client._delete("subaccounts", requestBody)
	if err != nil {
		log.Printf("Error DeleteSubaccount", err)
		return deleteSubaccount, err
	}
	err = _processResponse(resp, &deleteSubaccount)
	return deleteSubaccount, err
}

func (client *FtxClient) GetSubaccountBalances(nickname string) (structs.SubaccountBalances, error) {
	var subaccountBalances structs.SubaccountBalances
	resp, err := client._get("subaccounts/"+nickname+"/balances", []byte(""))
	if err != nil {
		log.Printf("Error SubaccountBalances", err)
		return subaccountBalances, err
	}
	err = _processResponse(resp, &subaccountBalances)
	return subaccountBalances, err
}

func (client *FtxClient) TransferSubaccounts(coin string, size float64, source string, destination string) (structs.TransferSubaccounts, error) {
	var transferSubaccounts structs.TransferSubaccounts
	requestBody, err := json.Marshal(map[string]interface{}{
		"coin":        coin,
		"size":        size,
		"source":      source,
		"destination": destination,
	})
	if err != nil {
		log.Printf("Error TransferSubaccounts", err)
		return transferSubaccounts, err
	}
	resp, err := client._post("subaccounts/transfer", requestBody)
	if err != nil {
		log.Printf("Error TransferSubaccounts", err)
		return transferSubaccounts, err
	}
	err = _processResponse(resp, &transferSubaccounts)
	return transferSubaccounts, err
}
