package ftx

import (
	"ftxbot/ftxexchange/ftx/structs"
	"log"
	"strconv"
)

func (client *FtxClient) GetMarkets() (structs.Markets, error) {
	var markets structs.Markets
	resp, err := client._get("markets", []byte(""))
	if err != nil {
		log.Printf("Error GetMarkets", err)
		return markets, err
	}
	err = _processResponse(resp, &markets)
	return markets, err
}

func (client *FtxClient) GetHistoricalPrices(market string, resolution int64,
	limit int64, startTime int64, endTime int64) (structs.HistoricalPrices, error) {
	var historicalPrices structs.HistoricalPrices
	resp, err := client._get(
		"markets/"+market+
			"/candles?resolution="+strconv.FormatInt(resolution, 10)+
			"&limit="+strconv.FormatInt(limit, 10)+
			"&start_time="+strconv.FormatInt(startTime, 10)+
			"&end_time="+strconv.FormatInt(endTime, 10),
		[]byte(""))
	if err != nil {
		log.Printf("Error GetHistoricalPrices", err)
		return historicalPrices, err
	}
	err = _processResponse(resp, &historicalPrices)
	return historicalPrices, err
}

func (client *FtxClient) GetTrades(market string, limit int64, startTime int64, endTime int64) (structs.Trades, error) {
	var trades structs.Trades
	resp, err := client._get(
		"markets/"+market+"/trades?"+
			"&limit="+strconv.FormatInt(limit, 10)+
			"&start_time="+strconv.FormatInt(startTime, 10)+
			"&end_time="+strconv.FormatInt(endTime, 10),
		[]byte(""))
	if err != nil {
		log.Printf("Error GetTrades", err)
		return trades, err
	}
	err = _processResponse(resp, &trades)
	return trades, err
}
