package ftx

import (
	"ftxbot/ftxexchange/ftx/structs"
	"log"
)

func (client *FtxClient) GetFuture(market string) (structs.Future, error) {
	var future structs.Future
	resp, err := client._get(
		"futures/"+market,
		[]byte(""))
	if err != nil {
		log.Printf("Error GetFuture", err)
		return future, err
	}
	err = _processResponse(resp, &future)
	return future, err
}
