package structs

import (
	"time"
)

type Markets struct {
	Success bool           `json:"success"`
	Result  []MargetResult `json:"result"`
}

type MargetResult struct {
	Name                  string  `json:"name"`
	BaseCurrency          string  `json:"baseCurrency"`
	QuoteCurrency         string  `json:"quoteCurrency"`
	QuoteVolume24h        float64 `json:"quoteVolume24h"`
	Change1h              float64 `json:"change1h"`
	Change24h             float64 `json:"change24h"`
	ChangeBod             float64 `json:"changeBod"`
	HighLeverageFeeExempt bool    `json:"highLeverageFeeExempt"`
	MinProvideSize        float64 `json:"minProvideSize"`
	Type                  string  `json:"type"`
	Underlying            string  `json:"underlying"`
	Enabled               bool    `json:"enabled"`
	Ask                   float64 `json:"ask"`
	Bid                   float64 `json:"bid"`
	Last                  float64 `json:"last"`
	PostOnly              bool    `json:"postOnly"`
	Price                 float64 `json:"price"`
	PriceIncrement        float64 `json:"priceIncrement"`
	SizeIncrement         float64 `json:"sizeIncrement"`
	Restricted            bool    `json:"restricted"`
	VolumeUsd24h          float64 `json:"volumeUsd24h"`
	LargeOrderThreshold   float64 `json:"largeOrderThreshold"`
}

type HistoricalPrices struct {
	Success bool `json:"success"`
	Result  []struct {
		Close     float64   `json:"close"`
		High      float64   `json:"high"`
		Low       float64   `json:"low"`
		Open      float64   `json:"open"`
		StartTime time.Time `json:"startTime"`
		Volume    float64   `json:"volume"`
	} `json:"result"`
}

type Trades struct {
	Success bool `json:"success"`
	Result  []struct {
		ID          int64     `json:"id"`
		Liquidation bool      `json:"liquidation"`
		Price       float64   `json:"price"`
		Side        string    `json:"side"`
		Size        float64   `json:"size"`
		Time        time.Time `json:"time"`
	} `json:"result"`
}
