package ftx

import (
	"ftxbot/ftxexchange/ftx/structs"
	"log"
)

func (client *FtxClient) GetBalances() (structs.Balances, error) {
	var balances structs.Balances
	resp, err := client._get("wallet/balances", []byte(""))
	if err != nil {
		log.Printf("Error GetBalances", err)
		return balances, err
	}
	err = _processResponse(resp, &balances)
	return balances, err
}
