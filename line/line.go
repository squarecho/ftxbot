package line

import (
	"fmt"

	notify "github.com/juunini/simple-go-line-notify/notify"
)

func Notify(message string, accessToken string) {
	if err := notify.SendText(accessToken, message); err != nil {
		fmt.Printf("%v", err)
	}
}
