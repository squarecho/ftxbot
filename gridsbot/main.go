package main

import (
	"encoding/json"
	"fmt"
	"ftxbot/gridsbot/gbot"
	"os"
	"time"
)

func main() {
	f, err := os.ReadFile("private_tester.json")
	if err != nil {
		fmt.Printf("%v", err)
		return
	}
	sets := &[]*gbot.SettingsInfo{}
	json.Unmarshal(f, sets)

	startat := time.Now()

	gs := make([]*gbot.Gbot, len(*sets))
	for i, set := range *sets {
		gs[i] = gbot.NewGbot(set)
		go gs[i].GridsRun()
		time.Sleep(2 * time.Second)
	}

	for range time.Tick(600 * time.Second) {
		fmt.Println(startat, time.Now().Sub(startat))
	}

}
